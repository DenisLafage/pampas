# Library

library(tidyverse)

# Import

com_plants <- read.csv('./csv/phyto2.csv')

d_pampas <- read_csv("./csv/data_pampas.csv") %>% 
  rowwise() %>% 
  mutate(m_AD = mean(c(R_spe_estim_Jack1_cor, R_spe_estim_Jack2_cor)),
         m_FD = mean(c(R_fonctio_estim_Jack1_cor, R_fonctio_estim_Jack2_cor)),
         m_PD = mean(c(R_phylo_estim_Jack1_cor, R_phylo_estim_Jack2_cor)),
         m_AD_car = mean(c(R_spe_car_Jack1_cor, R_spe_car_Jack2_cor)),
         m_FD_car = mean(c(R_fonctio_car_Jack1_cor, R_fonctio_car_Jack2_cor)),
         m_PD_car = mean(c(R_phylo_car_Jack1_cor, R_phylo_car_Jack2_cor))) %>% 
  select(-c(R_spe_estim_Jack1_cor, R_spe_estim_Jack2_cor,
            R_fonctio_estim_Jack1_cor, R_fonctio_estim_Jack2_cor, 
            R_phylo_estim_Jack1_cor, R_phylo_estim_Jack2_cor,
            R_spe_car_Jack1_cor, R_spe_car_Jack2_cor,
            R_fonctio_car_Jack1_cor, R_fonctio_car_Jack2_cor,
            R_phylo_car_Jack1_cor, R_phylo_car_Jack2_cor)) %>% 
  left_join(com_plants[,c(1,5:9,15)], by = 'num_piege')



ellen <- read_csv("./csv/ellenberg_values.csv") %>% 
  dplyr::select(LB_NOM, HILL_L, HILL_F, HILL_R, HILL_N, HILL_S) 

ellen$LB_NOM <- str_replace_all(ellen$LB_NOM, " ", "_")
